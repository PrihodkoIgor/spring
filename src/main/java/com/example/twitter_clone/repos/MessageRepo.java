package com.example.twitter_clone.repos;

import com.example.twitter_clone.domain.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepo extends CrudRepository <Message, Long> {

    List<Message> findByTag(String tag);
}